﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 

[System.Serializable]
public class AxleCarInfo {
	public WheelCollider leftWheel, rightWheel; 
	public GameObject leftWheelMesh, rightWheelMesh; 
	public bool motor, steering; 
}

public class DrivingSystem:MonoBehaviour {
	public List < AxleCarInfo > axleInfos; 
	public float maxMotorTorque; 
	public float maxSteeringAngle; 
	public float maxWheelRotation; 
	// Use this for initialization
	void Start () {
    	//transform.GetComponent<Rigidbody>().centerOfMass += Vector3(0, 0, 1.0f);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float motor = maxMotorTorque * Input.GetAxis("Vertical") * 2; 
		float steering = maxSteeringAngle * Input.GetAxis("Horizontal"); 
		foreach (AxleCarInfo axleInfo in axleInfos) {
			//Debug.Log(axleInfo.leftWheel.rpm);
			if (axleInfo.steering) {
				axleInfo.leftWheel.steerAngle = (steering/ (axleInfo.leftWheel.rpm + 1)) * 100; 
				axleInfo.rightWheel.steerAngle = (steering/ (axleInfo.rightWheel.rpm + 1)) * 100; 
			}
			//Debug.Log(axleInfo.leftWheel.steerAngle); 
			if (axleInfo.motor && axleInfo.leftWheel.rpm < maxWheelRotation) {
				axleInfo.leftWheel.motorTorque = motor; 
				axleInfo.rightWheel.motorTorque = motor; 
			}
			if (axleInfo.leftWheel.rpm > maxWheelRotation) {
				axleInfo.leftWheel.motorTorque = 0; 
				axleInfo.rightWheel.motorTorque = 0; 
			}
			ApplyLocalPositionToVisuals(axleInfo); 
			WheelHit hit; 
			float travelL = 1.0f; 
			float travelR = 1.0f; 
			float AntiRoll = 5000.0f; 
		
			bool groundedL = axleInfo.leftWheel.GetGroundHit(out hit); 
			if (groundedL)
				travelL = ( - axleInfo.leftWheel.transform.InverseTransformPoint(hit.point).y - axleInfo.leftWheel.radius)/axleInfo.leftWheel.suspensionDistance; 
		
			bool groundedR = axleInfo.rightWheel.GetGroundHit(out hit); 
			if (groundedR)
				travelR = ( - axleInfo.rightWheel.transform.InverseTransformPoint(hit.point).y - axleInfo.rightWheel.radius)/axleInfo.rightWheel.suspensionDistance; 
		
			float antiRollForce = (travelL - travelR) * AntiRoll; 
		
			if (groundedL) {
				transform.GetComponent < Rigidbody > ().AddForceAtPosition(axleInfo.leftWheel.transform.up *  - antiRollForce, axleInfo.leftWheel.transform.position); 
			}
			if (groundedR) {
				transform.GetComponent < Rigidbody > ().AddForceAtPosition(axleInfo.rightWheel.transform.up * antiRollForce, axleInfo.rightWheel.transform.position); 
			}
		}
	}
	public void ApplyLocalPositionToVisuals(AxleCarInfo wheelPair) {
		//wheelPair.leftWheelMesh.transform.Rotate(Vector3.up, Time.deltaTime * wheelPair.leftWheel.rpm*10, Space.Self);
		//wheelPair.rightWheelMesh.transform.Rotate(Vector3.up, Time.deltaTime * wheelPair.rightWheel.rpm*10, Space.Self);

		Quaternion quatLeft, quatRight; 
		Vector3 posLeft, posRight; 
		wheelPair.leftWheel.GetWorldPose(out posLeft, out quatLeft); 
		wheelPair.rightWheel.GetWorldPose(out posRight, out quatRight); 
		wheelPair.leftWheelMesh.transform.position = posLeft; 
		wheelPair.leftWheelMesh.transform.rotation = quatLeft; 
		wheelPair.leftWheelMesh.transform.Rotate(0, 0, 90); 
		wheelPair.leftWheelMesh.transform.Translate(0, -0.2f, 0); 
		wheelPair.rightWheelMesh.transform.position = posRight; 
		wheelPair.rightWheelMesh.transform.rotation = quatRight; 
		wheelPair.rightWheelMesh.transform.Rotate(0, -180, 90); 
		wheelPair.rightWheelMesh.transform.Translate(0, -0.2f, 0); 
		//Debug.Log(wheelPair.leftWheel.rpm);
	}
}
