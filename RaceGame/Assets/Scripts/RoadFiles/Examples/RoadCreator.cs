﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 

[RequireComponent(typeof(PathCreator))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class RoadCreator:MonoBehaviour {

    [Range(.05f, 1.5f)]
    public float spacing = 1; 
    public float roadWidth = 1; 
    public bool autoUpdate; 
    public float tiling = 1; 

    public float roadBorderWidth = 3;

    public Vector2[] points;

    public void UpdateRoad() {
        Path path = GetComponent < PathCreator > ().path; 
        points = path.CalculateEvenlySpacedPoints(spacing); 
        GetComponent < MeshFilter > ().mesh = CreateRoadMesh(points, path.IsClosed); 

        int textureRepeat = Mathf.RoundToInt(tiling * points.Length * spacing * .05f); 
        GetComponent < MeshRenderer > ().sharedMaterial.mainTextureScale = new Vector2(1, textureRepeat); 

        GameObject.Find("RoadBorderRight").GetComponent < MeshFilter > ().mesh = CreateRoadBorder(points, path.IsClosed, false); 
        GameObject.Find("RoadBorderLeft").GetComponent < MeshFilter > ().mesh = CreateRoadBorder(points, path.IsClosed, true); 
        DestroyImmediate(GameObject.Find("RoadBorderRight").GetComponent<MeshCollider>());
        DestroyImmediate(GameObject.Find("RoadBorderLeft").GetComponent<MeshCollider>());
        GameObject.Find("RoadBorderRight").AddComponent<MeshCollider>();
        GameObject.Find("RoadBorderLeft").AddComponent<MeshCollider>();
        //GameObject.Find("RoadBorderRight").GetComponent < MeshRenderer > ().sharedMaterial.mainTextureScale = new Vector3(textureRepeat, textureRepeat, textureRepeat); 
        //GameObject.Find("RoadBorderLeft").GetComponent < MeshRenderer > ().sharedMaterial.mainTextureScale = new Vector3(textureRepeat, textureRepeat, textureRepeat);  
    }

    //=====================CHANGES
    Mesh CreateRoadBorder(Vector2[] points, bool isClosed, bool isLeft) {
        Vector3[] verts = new Vector3[points.Length * 4]; 
        Vector2[] uvs = new Vector2[verts.Length]; 
        int numTris = 4 * (points.Length - 1) + ((isClosed)?2:0); 
        int[] tris = new int[numTris * 6]; 
        int vertIndex = 0; 
        int triIndex = 0; 

        for (int i = 0; i < points.Length; i++) {
            Vector2 forward = Vector2.zero; 
            if (i < points.Length - 1 || isClosed) {
                forward += points[(i + 1) % points.Length] - points[i]; 
            }
            if (i > 0 || isClosed) {
                forward += points[i] - points[(i - 1 + points.Length) % points.Length]; 
            }

            forward.Normalize(); 
            float sign = -1;
            Vector3 left = new Vector3( - forward.y, 0, forward.x); 
            if(isLeft)
               sign = 1;
            
            left*= sign;

            verts[vertIndex].x = points[i].x + left.x * roadWidth/roadBorderWidth * .5f;
            verts[vertIndex].y = 0;
            verts[vertIndex].z = points[i].y + left.z * roadWidth/roadBorderWidth * .5f;

            verts[vertIndex+1].x = points[i].x + left.x * roadWidth/roadBorderWidth * .5f + 5 * sign;
            verts[vertIndex+1].y = 0;
            verts[vertIndex+1].z = points[i].y + left.z * roadWidth/roadBorderWidth * .5f;

            verts[vertIndex+2].x = points[i].x + left.x * roadWidth/roadBorderWidth * .5f + 5 * sign;
            verts[vertIndex+2].y = 2;
            verts[vertIndex+2].z = points[i].y + left.z * roadWidth/roadBorderWidth * .5f;

            verts[vertIndex+3].x = points[i].x + left.x * roadWidth/roadBorderWidth * .5f;
            verts[vertIndex+3].y = 2;
            verts[vertIndex+3].z = points[i].y + left.z * roadWidth/roadBorderWidth * .5f;

            float completionPercent = i / (float)(points.Length - 1); 
            float v = 1 - Mathf.Abs(4 * completionPercent - 1); 
            uvs[vertIndex] = new Vector2(0, v); 
            uvs[vertIndex + 1] = new Vector2(1, v); 

            if (i < points.Length - 1 || isClosed) {
				tris[triIndex] = vertIndex; 
                tris[triIndex + 1] = vertIndex + 4;
				tris[triIndex + 2] = vertIndex + 7; 

				tris[triIndex + 3] = vertIndex; 
                tris[triIndex + 4] = vertIndex + 3;
                tris[triIndex + 5] = vertIndex + 7; 
                
                tris[triIndex + 6] = vertIndex + 3; 
                tris[triIndex + 7] = vertIndex + 2; 
                tris[triIndex + 8] = vertIndex + 6; 

                tris[triIndex + 9] = vertIndex + 3; 
                tris[triIndex + 10] = vertIndex + 7; 
                tris[triIndex + 11] =vertIndex + 6; 

                tris[triIndex + 12] = vertIndex + 6; 
                tris[triIndex + 13] = vertIndex + 2; 
                tris[triIndex + 14] = vertIndex + 1; 

                tris[triIndex + 15] = vertIndex + 6; 
                tris[triIndex + 16] = vertIndex + 5; 
                tris[triIndex + 17] =vertIndex + 1; 
            }

            vertIndex += 4; 
            triIndex += 18; 
        }

        Mesh mesh = new Mesh(); 
        mesh.vertices = verts; 
        mesh.triangles = tris; 
        mesh.uv = uvs; 

        return mesh; 
    }
    //=====================CHANGES ^^^

    Mesh CreateRoadMesh(Vector2[] points, bool isClosed) {
        Vector3[] verts = new Vector3[points.Length * 2]; 
        Vector2[] uvs = new Vector2[verts.Length]; 
        int numTris = 2 * (points.Length - 1) + ((isClosed)?2:0); 
        int[] tris = new int[numTris * 3]; 
        int vertIndex = 0; 
        int triIndex = 0; 

        for (int i = 0; i < points.Length; i++) {
            Vector2 forward = Vector2.zero; 
            if (i < points.Length - 1 || isClosed) {
                forward += points[(i + 1) % points.Length] - points[i]; 
            }
            if (i > 0 || isClosed) {
                forward += points[i] - points[(i - 1 + points.Length) % points.Length]; 
            }

            forward.Normalize(); 
            Vector2 left = new Vector2( - forward.y, forward.x); 

            verts[vertIndex] = points[i] + left * roadWidth * .5f; 
            verts[vertIndex + 1] = points[i] - left * roadWidth * .5f; 

            float completionPercent = i / (float)(points.Length - 1); 
            float v = 1 - Mathf.Abs(2 * completionPercent - 1); 
            uvs[vertIndex] = new Vector2(0, v); 
            uvs[vertIndex + 1] = new Vector2(1, v); 

            if (i < points.Length - 1 || isClosed) {
				tris[triIndex] = vertIndex; 
                tris[triIndex + 1] = (vertIndex + 2) % verts.Length; 
				tris[triIndex + 2] = vertIndex + 1; 

				tris[triIndex + 3] = vertIndex + 1; 
                tris[triIndex + 4] = (vertIndex + 2) % verts.Length; 
                tris[triIndex + 5] = (vertIndex + 3) % verts.Length; 
            }

            vertIndex += 2; 
            triIndex += 6; 
        }

        Mesh mesh = new Mesh(); 
        mesh.vertices = verts; 
        mesh.triangles = tris; 
        mesh.uv = uvs; 
        

        return mesh; 
    }
}